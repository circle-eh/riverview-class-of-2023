---
layout: page
title: 2023 Winter Fundraising Raffle
image: /assets/img/riverview.jpg
---

## Welcome to the 2023 Riverview Elementary Graduating Class Fundraising Raffle!

We are excited to announce our raffle event with six amazing prizes to be won,
each with a unique experience or theme. All proceeds from this event will go
towards supporting the 2023 graduating class of Riverview Elementary.

## Here are the prizes you could win

1. "[Hello
   Beautiful](https://checkout.square.site/buy/5PCWQ7E2YEL5IMTJCHHY74AA)"
   (valued at $100) - Treat yourself to some self-care with this package that
   includes a 30-minute massage, 10oz Bath & Body Works Shea Body Wash, 10oz
   Bath & Body Works Shea Body Cream, and a notebook. [Purchase
   Tickets](https://checkout.square.site/buy/5PCWQ7E2YEL5IMTJCHHY74AA)
1. "[Dimensions of
   Calm](https://checkout.square.site/buy/YRHHZUQ3YABROKVYY3ML36CF)" (valued at
   $100) - Relax and unwind with this package that includes a 30-minute massage,
   8oz Bath and Body Works Body Wash, 8oz Bath and Body Works Body Lotion,
   Silicone nail brush, and a hair comb. [Purchase
   Tickets](https://checkout.square.site/buy/YRHHZUQ3YABROKVYY3ML36CF)
1. "[Un, Deux, Trois!
   Boom!](https://checkout.square.site/buy/SQ2VNOAQNQRGQI5YFO7JD23X)" (valued at
   $80) - Learn and have fun with this educational package that includes a
   National Geographic Build Your Own Volcano Kit, 2 French Language Dr. Seuss
   books, a skipping rope, and a plushie. [Purchase
   Tickets](https://checkout.square.site/buy/SQ2VNOAQNQRGQI5YFO7JD23X)
1. "[Power of Past, Present and
   Future](https://checkout.square.site/buy/ILACEPEGDQWQ5PX5AQI4QF4T)" (valued
   at $80) - Explore the past, present, and future with this package that
   includes a Discovery Robot Building Kit, the novel "The Moon Within", the
   novel "The Witch Boy", and a Minions Bluetooth Speaker. [Purchase
   Tickets](https://checkout.square.site/buy/ILACEPEGDQWQ5PX5AQI4QF4T)
1. "[Pizza, Popcorn and a
   Movie!](https://checkout.square.site/buy/TPVSIBODEDXFW674JTR2ZTM3)" (valued
   at $55) - Enjoy a night in with this package that includes a restaurant gift
   card of $25 for Restaurant Nouveau Verdun, a 3 pack of Kernels Popcorn
   Seasoning, a back of popcorn kernels, and a $10 gift card to Restaurant
   Bingo. [Purchase
   Tickets](https://checkout.square.site/buy/TPVSIBODEDXFW674JTR2ZTM3)
1. "[Brunch in
   Verdun!](https://checkout.square.site/buy/DJ6OQSADFYBIBQDZKVQAXZNZ)" (valued
   at $55) - Treat yourself to a brunch in Verdun with this package that
   includes a $20 gift card to Bagel St. Lo, a $10 gift card to Restaurant
   Bingo, and a $25 gift card to Café Chato. [Purchase
   Tickets](https://checkout.square.site/buy/DJ6OQSADFYBIBQDZKVQAXZNZ)

To participate in the raffle, simply purchase a ticket for $2 each by clicking
the name of the package you are interested in. The more tickets you purchase,
the greater your chances of winning one of these fantastic prizes. The winners
will be announced on the 2023 Riverview Elementary Graduating Class Fundraising
Raffle website on **February 28th, 2023**.

Don't miss this opportunity to support the 2023 graduating class of Riverview
Elementary and win big at the same time! Get your tickets now!
