---
layout: page
title: 2023 Spring Fundraising Raffle
image: assets/img/riverview.jpg
description: >
  Welcome to the final Riverview Elementary School graduation fundraiser raffle!
  We're excited to offer some amazing prizes to help raise money for our school.
  Each ticket costs just $2, so be sure to get yours today!
---

## Welcome to the 2023 Riverview Elementary Graduating Class Fundraising Raffle!

Welcome to final Grad raffle of the year! We're excited to offer some amazing
prizes to help raise money for the graduating class of 2023. Each ticket costs
just $2, so be sure to get yours today!

{% include donate.html %}

Check out our fantastic prizes:

- Canadian Olympic Team branded jacket (Woman's 2XS) and pants (W29/L34)
- A $10 gift card to Restaurant Bingo, one of Verdun's hidden gems. This OG diner
  serves up classic casse croute food food at unbeatable prices.
- A $20 gift card to Bagel St.-Lo. Bagel St-Lo first opened its doors July 2014
  and continues to serve fresh bagels, baked onsite, as well as healthy
  breakfasts and lunches, right in the heart of Verdun. This
  eclectically-decorated little neighbourhood restaurant will charm you with its
  warm welcome and its homey atmosphere.
- $25 gift card to Antidote Bouffe Végane. Antidote is an extraordinary bistro
  with a magnificent and decadent menu. At Antidote, nothing stops their cooks
  in order to innovate more, the menu changes according to the season, whether
  it's a mac n'cheese with three cheeses (vegan of course!), a plate of seitan
  in a crust, or their famous crazy burger!
- A $25 gift card for Pharmaprix.
- A second $25 gift card for Pharmaprix.
- [National Geographic Air Rocket kit](https://youtu.be/e08qUnZjIcA), valued at
  $40.
- An [osteopathy session with Julie Lapointe](https://energina.ca/ostheopathie/).
- 60 minute massage with [Sandra Lambert](https://energina.ca/massotherapie/).
- A [Neuralign Gift Card](https://lsworks.org/) (value $1500). This is a game-changing
  evidence-based gamified cognitive development program. Created by a
  neurodiverse team to empower the neurodiverse community, Neuralign is based on
  the most advanced principles of neuroscience and neural plasticity, with years
  of research and fieldwork.

These prizes are sure to make anyone excited!

Don't miss your chance to win one of these great prizes! Get [your tickets
now](https://www.paypal.com/pools/c/8TFuboWaCW) and help support our grads.
Thank you for your participation!

{% include donate.html %}

## FAQ

1. **When is the draw?** The draw will be done on May 15th, and the results
   shared once the winners have been notified.
1. **How will the draw be done?** We will draw 1 name per prize from the pool of
   tickets. The draws will happen in the order in which the prizes are listed
   above. Once a ticket has been drawn, it will not qualify for a second prize.
1. **How can I enter?** Entries will be automatically made for any online
   [donations](https://www.paypal.com/pools/c/8TFuboWaCW) at a rate of 1 ticket
   for every $2 donated.
